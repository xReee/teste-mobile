//
//  InicialMainView.swift
//  Test Mobile
//
//  Created by Renata Faria on 27/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

final class SearchView: UIView {
    weak var delegate: SearchViewInputDelegate?
    var isCompressed = false {
        didSet {
            self.animateConstraints()
        }
    }
    
    convenience init(isCompressed: Bool) {
        self.init()
        self.isCompressed = isCompressed
    }
    func clearSearch() {
        searchBarTextField.text = ""
    }
    // #MARK: Views
    var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "madeinweb")
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    var searchButton: UIButton = {
        let button = UIButton()
        button.setTitle("Buscar", for: .normal)
        button.backgroundColor = AppColor.blue
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    var searchBarTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Pesquisar"
        textField.returnKeyType = .search
        textField.layer.cornerRadius = 5
        textField.layer.borderWidth = 2
        textField.layer.borderColor = AppColor.gray.cgColor
        textField.clipsToBounds = true
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    
    // #MARK: Actions
    @objc func clickedSearch(_ sender: UIButton) {
        searchBarTextField.resignFirstResponder()
        let notification = UINotificationFeedbackGenerator()
        if let text = self.searchBarTextField.text, !text.isEmpty {
            notification.notificationOccurred(.success)
            sender.correctAnimation()
            delegate?.didSearch(text: text)
        } else {
            notification.notificationOccurred(.error)
            sender.wrongAnimation()
        }
    }
   
}
extension SearchView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let notification = UINotificationFeedbackGenerator()
        if let text = self.searchBarTextField.text, !text.isEmpty {
            notification.notificationOccurred(.success)
            delegate?.didSearch(text: text)
        } else {
            notification.notificationOccurred(.error)
            textField.wrongAnimation()
        }
        return true
    }
}
