//
//  InitialViewProtocols.swift
//  Test Mobile
//
//  Created by Renata Faria on 29/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

protocol InitialViewInputViewDelegate: class {
    func didSearch(text: String)
    func didSelected(video: Video)
}

protocol SearchViewInputDelegate: class {
    func didSearch(text: String)
}

protocol InitialViewOutputControllerDelegate: class {
    func didFound(videos: [Video])
}

protocol InitialViewOutputViewDelegate: class {
    func didTappedNavBar()
}

protocol InitialViewOutputModelDelegate: class {
    func didFound(videos: [Video])
    func didFoundError(description: String)
}


