//
//  UIButton+Animation.swift
//  Test Mobile
//
//  Created by Renata Faria on 28/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

extension UIView {
    func correctAnimation() {
           let pulse = CASpringAnimation(keyPath: "transform.scale")
           pulse.duration = 0.2
           pulse.fromValue = 0.95
           pulse.toValue = 1.0
           pulse.initialVelocity = 0.5
           pulse.damping = 1.0
           layer.add(pulse, forKey: "pulse")
   }
    func wrongAnimation() {
        let shake = CABasicAnimation(keyPath: "position")
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        shake.fromValue = fromValue
        shake.toValue = toValue
        shake.duration = 0.1
        shake.repeatCount = 4
        shake.autoreverses = true
        
        let blink = CABasicAnimation(keyPath: "backgroundColor")
        blink.fromValue = self.backgroundColor
        blink.toValue = UIColor.systemRed.cgColor
        blink.duration = 0.3
        
        let group = CAAnimationGroup()
        group.animations = [shake, blink]
        group.duration = 0.4
        
        layer.add(group, forKey: "nil")
    }
}
