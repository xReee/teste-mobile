//
//  InitialMainView+DataSource.swift
//  Test Mobile
//
//  Created by Renata Faria on 28/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

extension InitialMainView: UITableViewDataSource {
    func configureTableView() {
        tableView.register(VideoTableViewCell.self, forCellReuseIdentifier: "1")
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "1") as? VideoTableViewCell else {
            return UITableViewCell()
        }
        cell.setup(video: videos[indexPath.row])
        return cell
    }
}


extension InitialMainView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        inputDelegate?.didSelected(video: videos[indexPath.row])
    }
}
