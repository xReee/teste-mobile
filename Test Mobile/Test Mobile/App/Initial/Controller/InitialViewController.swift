//
//  InitialViewController.swift
//  Test Mobile
//
//  Created by Renata Faria on 27/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

final class InitialViewController: UIViewController {
    fileprivate lazy var initialView = InitialMainView(mainView: self.view)
    fileprivate lazy var dataModel = InitialViewModel()
    fileprivate weak var modelOutputDelegate: InitialViewOutputControllerDelegate?
    fileprivate weak var viewOutputDelegate: InitialViewOutputViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavBar()
        initialView.inputDelegate = self
        viewOutputDelegate = initialView
        dataModel.delegate = self
        modelOutputDelegate = initialView
        initialView.setupView()
    }
    func configureNavBar() {
        self.navigationController?.navigationBar.isHidden = true
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "madeinweb.png")
        imageView.image = image
        
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.tappedLogo))
        logoContainer.isUserInteractionEnabled = true
        logoContainer.addGestureRecognizer(recognizer)
    }
    @objc func tappedLogo() {
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
        viewOutputDelegate?.didTappedNavBar()
    }
}

extension InitialViewController: InitialViewInputViewDelegate {
    func didSelected(video: Video) {
        let detailView = DetailsViewController()
        detailView.video = video
        self.navigationController?.pushViewController(detailView, animated: true)
    }
    
    func didSearch(text: String) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        dataModel.makeRequest(with: text)
    }
}

extension InitialViewController: InitialViewOutputModelDelegate {
    func didFoundError(description: String) {
        let alert = UIAlertController(title: "Ooops", message: "Algo de errado com a API.\n Erro: \(description)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func didFound(videos: [Video]) {
        modelOutputDelegate?.didFound(videos: videos)
    }
}
