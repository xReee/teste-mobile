//
//  UIColor+AppColors.swift
//  Test Mobile
//
//  Created by Renata Faria on 28/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

struct AppColor {
    static var blue: UIColor {
        return UIColor(named: "appBlue") ?? UIColor.systemBlue
    }
    static var gray: UIColor {
        return UIColor(named: "appGray") ?? UIColor.systemBlue
    }
}
