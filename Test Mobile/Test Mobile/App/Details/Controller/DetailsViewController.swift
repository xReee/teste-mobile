//
//  File.swift
//  Test Mobile
//
//  Created by Renata Faria on 29/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

final class DetailsViewController: UIViewController {
    // MARK: - Properties
    fileprivate lazy var initialView = DetailsView(mainView: self.view, video: video)
    fileprivate lazy var dataModel = DetailsModel()
    fileprivate var modelOutputDelegate: DetailsOutputModelDelegate?
    var video: Video!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavBarImage()
        self.configureNavBarButton()
        initialView.setupView()
        dataModel.getViews(of: self.video)
        dataModel.delegate = self
        modelOutputDelegate = initialView
    }
    
    // MARK: - Methods
    func configureNavBarImage() {
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "madeinweb.png")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    func configureNavBarButton() {
        let backImage = UIImage(named: "voltar")
        self.navigationController?.navigationBar.tintColor = AppColor.gray
        self.navigationController?.navigationBar.backIndicatorImage = backImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        self.navigationController?.navigationBar.backItem?.title = "Voltar"
    }
}
extension DetailsViewController: DetailsOutputModelDelegate {
    func didFoundError(description: String) {
        let alert = UIAlertController(title: "Ooops", message: "Algo de errado com a API.\nErro: \(description)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func didFound(numberOf viewsCount: String) {
        modelOutputDelegate?.didFound(numberOf: viewsCount)
    }
}
