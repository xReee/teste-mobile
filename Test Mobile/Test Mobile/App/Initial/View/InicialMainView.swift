//
//  InicialMainView.swift
//  Test Mobile
//
//  Created by Renata Faria on 27/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

class InitialMainView: UIView {
    
    weak var mainView: UIView?
    
    convenience init(mainView: UIView) {
           self.init()
           self.mainView = mainView
    }
    var logoImage: UIImage = {
        let image = UIImage()
        
    }()
}

extension InitialMainView: ViewCode {
    func buildView() {
        //
    }
    
    func setupConstraints() {
        //
    }
    
    func setupAdditionalConfigs() {
        mainView?.backgroundColor = .blue
    }
}
