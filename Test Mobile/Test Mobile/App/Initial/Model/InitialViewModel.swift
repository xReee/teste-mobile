//
//  InitialViewModel.swift
//  Test Mobile
//
//  Created by Renata Faria on 28/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import Foundation

// #TODO: - refatorar SOS

final class InitialViewModel {
    weak var delegate: InitialViewOutputModelDelegate?
    var videos: [Video] = []
    func makeRequest(with parameter: String) {
        self.videos.removeAll() //change when add page requests
        guard var urlComponents = API.searchURL else { return }
        
        urlComponents.queryItems = [
            URLQueryItem(name: "part", value: "snippet"),
            URLQueryItem(name: "q", value: "\(parameter)"),
            URLQueryItem(name: "type", value: "video"),
            URLQueryItem(name: "key", value: "\(API.key)")
        ]
        
        let request = URLRequest(url: urlComponents.url!)
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request, completionHandler: { [unowned self] data, response, error -> Void in
            do {
                if let error = error {
                    self.delegate?.didFoundError(description: error.localizedDescription)
                    return
                }
                if let httpResponse = response as? HTTPURLResponse, !httpResponse.isResponseOK()  {
                    self.delegate?.didFoundError(description: "Resposta inesperada. Código \(httpResponse.statusCode)")
                    return
                }
                
                guard let data = data,
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject],
                    let items = jsonResult["items"] as? Array<[String:AnyObject]> else { return }
                
                for item in items {
                    let snippet = item["snippet"]
                    var thumbnailUrl = ""
                    if let thumbnailsArray = snippet?["thumbnails"] as? [String: Any],
                        let thumbnailArray = thumbnailsArray["high"] as? [String: Any],
                        let url = thumbnailArray["url"] as? String {
                        thumbnailUrl = url
                    }
                    if let title = snippet?["title"] as? String,
                        let description = snippet?["description"] as? String,
                        let videoID = item["id"]?["videoId"] as? String {
                        let video = Video(id: videoID, title: title, description: description, thumbnailUrl: thumbnailUrl)
                        self.videos.append(video)
                    }
                }
                self.delegate?.didFound(videos: self.videos)
            }
            catch {
                print("json error: \(error)")
            }
        })
        task.resume()
    }
}
