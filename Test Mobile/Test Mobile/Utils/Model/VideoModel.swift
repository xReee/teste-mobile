//
//  VideoModel.swift
//  Test Mobile
//
//  Created by Renata Faria on 28/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

struct Video {
    let id: String
    let title: String
    let description: String
    let thumbnailUrl: String
    
    var loadedThumbnail: UIImage? {
        if let url = URL(string: thumbnailUrl),
            let data = try? Data(contentsOf: url) {
            return UIImage(data: data)
        }
        return UIImage()
    }
    
}
