//
//  DetailsView.swift
//  Test Mobile
//
//  Created by Renata Faria on 29/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

final class DetailsView: UIView {
    weak var mainView: UIView?
    
    convenience init(mainView: UIView, video: Video) {
        self.init()
        self.mainView = mainView
        self.setupView()
        setupVideo(video: video)
    }
    func setupVideo(video: Video) {
        self.titleLabel.text = video.title
        //        self.descriptionView.text = video.description
        self.descriptionView.text = video.description
        DispatchQueue.main.async { [unowned self] in
            self.activityIndicator.stopAnimating()
            self.thumbnailImageView.image = video.loadedThumbnail
        }
    }
    func updateViewsCountLabel(to viewCount: String) {
        viewsLabel.text = viewCount
    }
    lazy var activityIndicator = UIActivityIndicatorView(style: .large)
    var thumbnailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    var titleLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Video Title"
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 20, weight: .bold)
        label.textColor = AppColor.blue
        return label
    }()
    var viewsLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 14, weight: .bold)
        label.text = "0 visualizações"
        label.textColor = AppColor.gray
        label.numberOfLines = 1
        return label
    }()
    var descriptionView: UITextView = {
        var view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .systemFont(ofSize: 14, weight: .regular)
        view.text = ""
        view.backgroundColor = .clear
        view.isEditable = false
        view.isSelectable = false
        return view
    }()
}

extension DetailsView: ViewCode {
    func buildView() {
        mainView?.addSubview(thumbnailImageView)
        mainView?.addSubview(titleLabel)
        mainView?.addSubview(viewsLabel)
        mainView?.addSubview(descriptionView)
        thumbnailImageView.addSubview(activityIndicator)
    }
    
    func setupConstraints() {
        guard let mainView = mainView else { return }
        setThumbnailConstraints(mainView)
        setTitleConstraints(mainView)
        setViewsConstraints(mainView)
        setDescriptionConstraints(mainView)
    }
    
    fileprivate func setThumbnailConstraints(_ mainView: UIView) {
        thumbnailImageView.widthAnchor.constrain(to: mainView.widthAnchor)
        thumbnailImageView.heightAnchor.constraint(equalTo: thumbnailImageView.widthAnchor, multiplier: 0.5).isActive = true
        thumbnailImageView.topAnchor.constrain(to: mainView.topAnchor)
        thumbnailImageView.centralizeXaxys(in: mainView)
        activityIndicator.centralizeXYaxys(in: thumbnailImageView)
    }
    fileprivate func setTitleConstraints(_ mainView: UIView) {
        titleLabel.topAnchor.constrain(to: thumbnailImageView.bottomAnchor, with: 15)
        titleLabel.leadingAnchor.constrain(to: mainView.leadingAnchor, with: 15)
        titleLabel.trailingAnchor.constrain(to: mainView.trailingAnchor, with: -15)
    }
    fileprivate func setViewsConstraints(_ mainView: UIView) {
        viewsLabel.topAnchor.constrain(to: titleLabel.bottomAnchor, with: 5)
        viewsLabel.leadingAnchor.constrain(to: titleLabel.leadingAnchor)
        viewsLabel.trailingAnchor.constrain(to: titleLabel.trailingAnchor)
    }
    fileprivate func setDescriptionConstraints(_ mainView: UIView) {
        descriptionView.topAnchor.constrain(to: viewsLabel.bottomAnchor, with: 5)
        descriptionView.leadingAnchor.constrain(to: titleLabel.leadingAnchor)
        descriptionView.trailingAnchor.constrain(to: titleLabel.trailingAnchor)
        descriptionView.bottomAnchor.constrain(to: mainView.bottomAnchor, with: -15)
    }
    
    func setupAdditionalConfigs() {
        activityIndicator.startAnimating()
        self.mainView?.backgroundColor = .systemBackground
    }
}

extension DetailsView: DetailsOutputModelDelegate {
    func didFoundError(description: String) {}

    func didFound(numberOf viewsCount: String) {
        DispatchQueue.main.async { [unowned self] in
            self.updateViewsCountLabel(to: viewsCount + " visualizações")
        }
        
    }
}
