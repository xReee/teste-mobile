//
//  InitialMainView.swift
//  Test Mobile
//
//  Created by Renata Faria on 28/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

final class InitialMainView: UIView {
    weak var mainView: UIView?
    weak var inputDelegate: InitialViewInputViewDelegate?
    
    fileprivate var isCompressed = false
    fileprivate var tableViewNoSizeConstraint = NSLayoutConstraint()
    fileprivate var tableViewDynamicSizeConstraint = NSLayoutConstraint()
    
    var videos: [Video] = [] {
        didSet {
            self.tableView.reloadData() //mudar isso pra insert quando der
        }
    }
    
    // MARK: LifeCycle
    convenience init(mainView: UIView) {
        self.init()
        self.mainView = mainView
        self.mainView?.backgroundColor = .systemBackground
        self.searchView.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    // MARK:  Views
    var searchView: SearchView = {
        let searchView = SearchView(isCompressed: false)
        return searchView
    }()
    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
        
}
extension InitialMainView: ViewCode {
    func buildView() {
        self.mainView?.addSubview(searchView)
        self.mainView?.addSubview(tableView)
    }
    
    func setupConstraints() {
        guard let mainView = mainView else { return }
        searchView.centralizeXaxys(in: mainView)
        searchView.widthAnchor.constrainAnchor(to: mainView.widthAnchor)
        searchView.topAnchor.constrain(to: mainView.topAnchor)
        searchView.bottomAnchor.constrain(to: tableView.topAnchor)
        
        tableView.bottomAnchor.constrain(to: mainView.bottomAnchor)
        tableView.widthAnchor.constrainAnchor(to: mainView.widthAnchor)
        tableViewNoSizeConstraint = tableView.heightAnchor.constraint(equalToConstant: 0)
        tableViewDynamicSizeConstraint = tableView.heightAnchor.constraint(equalTo: mainView.heightAnchor, multiplier: 0.9)
        
        tableViewDynamicSizeConstraint.isActive = false
        tableViewNoSizeConstraint.isActive = true
    }
    
    func setupAdditionalConfigs() {
        searchView.setupView()
            configureTableView()
    }
    fileprivate func setSearchBar(to compress: Bool) {
        tableViewNoSizeConstraint.isActive = !compress
        tableViewNoSizeConstraint.priority = compress ? .defaultLow : .required
        tableViewDynamicSizeConstraint.isActive = compress
        tableViewDynamicSizeConstraint.priority = compress ? .required : .defaultLow
        self.searchView.isCompressed = compress
        UIView.animate(withDuration: 0.5, animations: {
            self.searchView.layoutIfNeeded()
            self.layoutIfNeeded()
        })
    }
}

extension InitialMainView: SearchViewInputDelegate {
    func didSearch(text: String) {
        inputDelegate?.didSearch(text: text)
        setSearchBar(to: true)
    }
}

extension InitialMainView: InitialViewOutputControllerDelegate {
    func didFound(videos: [Video]) {
        DispatchQueue.main.async { [unowned self] in
            self.videos = videos
        }
    }
}
extension InitialMainView: InitialViewOutputViewDelegate {
    func didTappedNavBar() {
        self.searchView.clearSearch()
        self.videos = []
        setSearchBar(to: false)
    }
}
