//
//  DetailsModel.swift
//  Test Mobile
//
//  Created by Renata Faria on 29/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

final class DetailsModel {
    weak var delegate: DetailsOutputModelDelegate?
    
    func getViews(of video: Video) {
        guard var urlComponents = API.videosURL else { return }
        urlComponents.queryItems = [
            URLQueryItem(name: "part", value: "statistics"),
            URLQueryItem(name: "id", value: "\(video.id)"),
            URLQueryItem(name: "key", value: "\(API.key)")
        ]
        let request = URLRequest(url: urlComponents.url!)
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request, completionHandler: { [unowned self] data, response, error -> Void in
            do {
                if let error = error {
                    self.delegate?.didFoundError(description: error.localizedDescription)
                    return
                }
                if let httpResponse = response as? HTTPURLResponse, !httpResponse.isResponseOK()  {
                    self.delegate?.didFoundError(description: "Resposta inesperada. Código \(httpResponse.statusCode)")
                    return
                }
                guard let data = data,
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject],
                    let items = jsonResult["items"] as? Array<[String: AnyObject]>,
                    let item = items.first else { return }
                if let statistic = item["statistics"] as? [String: AnyObject],
                    let viewCount = statistic["viewCount"] as? String {
                    self.delegate?.didFound(numberOf: viewCount)
                }
            }
            catch {
                print("json error: \(error)")
            }
        })
        task.resume()
    }
}
