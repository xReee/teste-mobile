//
//  VideoTableViewCell.swift
//  Test Mobile
//
//  Created by Renata Faria on 29/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(video: Video) {
        titleLabel.text = video.title
        descriptionLabel.text = video.description
        self.thumbnailImageView.image = nil
        DispatchQueue.main.async { [unowned self] in
            self.activityIndicator.stopAnimating()
            self.thumbnailImageView.image = video.loadedThumbnail
        }
    }
    
    var thumbnailImageView: UIImageView = {
        var image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.layer.borderWidth = 2
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.layer.borderColor = AppColor.gray.cgColor
        return image
    }()
    var titleLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Video Title"
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 16, weight: .bold)
        label.textColor = AppColor.blue
        return label
    }()
    var descriptionLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 14, weight: .regular)
        label.text = ""
        label.numberOfLines = 0
        return label
    }()
    lazy var activityIndicator = UIActivityIndicatorView(style: .medium)
    
}

extension VideoTableViewCell: ViewCode {
    func buildView() {
        self.addSubview(thumbnailImageView)
        thumbnailImageView.addSubview(activityIndicator)
        self.addSubview(titleLabel)
        self.addSubview(descriptionLabel)
    }
    
    func setupConstraints() {
        configureThumbnailConstraints()
        configureTitleConstraints()
        configureDescriptionConstraints()
    }
    
    func setupAdditionalConfigs() {
        activityIndicator.startAnimating()
    }
    
    fileprivate func configureThumbnailConstraints() {
        thumbnailImageView.topAnchor.constrain(to: self.topAnchor, with: 15)
        thumbnailImageView.bottomAnchor.constrain(.lessThanOrEqual, to: self.bottomAnchor, with: -15)
        thumbnailImageView.leftAnchor.constrain(to: self.leftAnchor, with: 10)
        thumbnailImageView.heightAnchor.constrain(to: 85)
        thumbnailImageView.widthAnchor.constrain(to: 152)
        activityIndicator.centralizeXYaxys(in: thumbnailImageView)
    }
    fileprivate func configureTitleConstraints() {
        titleLabel.topAnchor.constrain(to: self.topAnchor, with: 15)
        titleLabel.leadingAnchor.constrain(to: thumbnailImageView.trailingAnchor, with: 15)
        titleLabel.trailingAnchor.constrain(to: self.trailingAnchor, with: -10)
    }
    fileprivate func configureDescriptionConstraints() {
        descriptionLabel.topAnchor.constrain(to: titleLabel.bottomAnchor, with: 2)
        descriptionLabel.leadingAnchor.constrain(to: titleLabel.leadingAnchor)
        descriptionLabel.trailingAnchor.constrain(to: titleLabel.trailingAnchor)
        descriptionLabel.bottomAnchor.constrain(to: self.bottomAnchor, with: -50, prioritizeAs: .defaultLow)
    }
}
