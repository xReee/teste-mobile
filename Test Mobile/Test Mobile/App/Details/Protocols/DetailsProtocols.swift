//
//  DetailsProtocols.swift
//  Test Mobile
//
//  Created by Renata Faria on 29/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import Foundation

protocol DetailsOutputModelDelegate: class {
    func didFound(numberOf viewsCount: String)
    func didFoundError(description: String)
}
