//
//  NSLayoutAnchor+Extension.swift
//  Test Mobile
//
//  Created by Renata Faria on 27/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//  A little help of: https://pedrommcarrasco.github.io/posts/power-up-your-anchors/

import Foundation
import UIKit

@objc extension NSLayoutAnchor {
    @discardableResult
    func constrain(_ relation: NSLayoutConstraint.Relation = .equal,
                    to anchor: NSLayoutAnchor,
                    with constant: CGFloat = 0.0,
                    prioritizeAs priority: UILayoutPriority = .required,
                    isActive: Bool = true) -> NSLayoutConstraint {

        var constraint = NSLayoutConstraint()

        switch relation {
            case .equal:
                constraint = self.constraint(equalTo: anchor, constant: constant)
            case .greaterThanOrEqual:
                constraint = self.constraint(greaterThanOrEqualTo: anchor, constant: constant)
            case .lessThanOrEqual:
                constraint = self.constraint(lessThanOrEqualTo: anchor, constant: constant)
            default: break
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
}

extension NSLayoutDimension {
    @discardableResult
    func constrain(_ relation: NSLayoutConstraint.Relation = .equal,
                   to constant: CGFloat = 0.0,
                   prioritizeAs priority: UILayoutPriority = .required,
                   isActive: Bool = true) -> NSLayoutConstraint {
        
        var constraint = NSLayoutConstraint()
        
        switch relation {
            case .equal:
                constraint = self.constraint(equalToConstant: constant)
            case .greaterThanOrEqual:
                constraint = self.constraint(greaterThanOrEqualToConstant: constant)
            case .lessThanOrEqual:
                constraint = self.constraint(lessThanOrEqualToConstant: constant)
            default: break
        }
        constraint.priority = priority
        constraint.isActive = isActive
        return constraint
    }
    @discardableResult
      func constrainAnchor(_ relation: NSLayoutConstraint.Relation = .equal,
                      to anchor: NSLayoutDimension,
                      with constant: CGFloat = 0.0,
                      prioritizeAs priority: UILayoutPriority = .required,
                      isActive: Bool = true) -> NSLayoutConstraint {

          var constraint = NSLayoutConstraint()

          switch relation {
              case .equal:
                  constraint = self.constraint(equalTo: anchor, constant: constant)
              case .greaterThanOrEqual:
                  constraint = self.constraint(greaterThanOrEqualTo: anchor, constant: constant)
              case .lessThanOrEqual:
                  constraint = self.constraint(lessThanOrEqualTo: anchor, constant: constant)
              default: break
          }
          constraint.priority = priority
          constraint.isActive = isActive
          return constraint
      }
}
