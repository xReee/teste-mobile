//
//  API.swift
//  Test Mobile
//
//  Created by Renata Faria on 29/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import Foundation

struct API {
    static var key: String {
        return "AIzaSyAvn62fsq7nckhJGKpZOmL21610Ski4zvM"
    }
    static var searchURL: URLComponents? {
            return URLComponents(string: "https://www.googleapis.com/youtube/v3/search")
    }
    static var videosURL: URLComponents? {
            return URLComponents(string: "https://www.googleapis.com/youtube/v3/videos")
    }
}
