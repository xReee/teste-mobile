//
//  InitialMainView+ViewCode.swift
//  Test Mobile
//
//  Created by Renata Faria on 28/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

extension SearchView: ViewCode {
    func buildView() {
        self.addSubview(logoImageView)
        self.addSubview(searchButton)
        self.addSubview(searchBarTextField)
    }
    
    func setupConstraints() {
        removeAllConstraints()
        activeLayoutConstraints()
        self.layoutIfNeeded()
    }
    func removeAllConstraints() {
        self.subviews.forEach { view in
            view.removeConstraints(view.constraints)
        }
        self.removeConstraints(self.constraints)
    }
    func animateConstraints() {
        removeAllConstraints()
        activeLayoutConstraints(false)
        activeLayoutConstraints(true)
        searchBarTextField.textAlignment = .natural
    }
    
    func setupAdditionalConfigs() {
        self.backgroundColor = .systemBackground
        searchBarTextField.textAlignment = .center
        searchBarTextField.delegate = self
        searchButton.addTarget(self, action: #selector(clickedSearch(_:)), for: .touchUpInside)
    }
    
    fileprivate func getConstraintsInSearchBar(for compressed: Bool) -> [NSLayoutConstraint] {
        var constraints = [NSLayoutConstraint]()
        //searchBar
        if !compressed {
            constraints = [
                searchBarTextField.getCentralizedXaxys(in: self),
                searchBarTextField.getCentralizedYaxys(in: self),
                searchBarTextField.leadingAnchor.constrain(.equal, to: self.leadingAnchor, with: 20, prioritizeAs: .defaultHigh, isActive: false),
                searchBarTextField.trailingAnchor.constrain(.equal, to: self.trailingAnchor, with: -20, prioritizeAs: .defaultHigh, isActive: false),
                searchBarTextField.widthAnchor.constrain(.lessThanOrEqual, to: 500, isActive: false),
                
            ]
        } else {
            constraints = [
                searchBarTextField.leadingAnchor.constrain(.equal, to: self.leadingAnchor, with: 20, prioritizeAs: .defaultHigh, isActive: false),
                searchBarTextField.trailingAnchor.constrain(.equal, to: self.trailingAnchor, with: -100, prioritizeAs: .defaultHigh, isActive: false),
                searchBarTextField.widthAnchor.constrain(.greaterThanOrEqual, to: 200, isActive: false),
                searchButton.topAnchor.constrain(to: self.topAnchor, with: 5, isActive: false),
                searchBarTextField.bottomAnchor.constrain(to: self.bottomAnchor, with: -10, isActive: isCompressed)
            ]
        }
        constraints.append(searchBarTextField.heightAnchor.constrain(to: 52, isActive: false))
        return constraints
    }
    
    fileprivate func getConstraintsInSearchButton(for compressed: Bool) -> [NSLayoutConstraint] {
        var constraints = [NSLayoutConstraint]()
        //searchButton
        if !compressed {
            constraints = [
                searchButton.getCentralizedXaxys(in: searchBarTextField),
                searchButton.topAnchor.constrain(to: searchBarTextField.bottomAnchor, with: 15, isActive: false),
                searchButton.widthAnchor.constrainAnchor(to: searchBarTextField.widthAnchor, isActive: false)
            ]
        } else {
            constraints = [
                searchButton.bottomAnchor.constrain(to: searchBarTextField.bottomAnchor, isActive: false),
                searchButton.leadingAnchor.constrain(to: searchBarTextField.trailingAnchor, isActive: false),
                searchButton.trailingAnchor.constrain(to: self.trailingAnchor, with: -20, isActive: false)
            ]
        }
        constraints.append(searchButton.heightAnchor.constrain(to: searchBarTextField.heightAnchor, isActive: false))
        return constraints
    }
    
    fileprivate func getConstraintsInLogoView(for compressed: Bool) -> [NSLayoutConstraint] {
        var constraints = [NSLayoutConstraint]()
        if !compressed {
            constraints = [
                logoImageView.leadingAnchor.constrain(to: self.leadingAnchor, with: 40, isActive: false),
                logoImageView.trailingAnchor.constrain(to: self.trailingAnchor, with: -40, isActive: false),
                logoImageView.bottomAnchor.constrain(to: searchBarTextField.topAnchor, with: -80, prioritizeAs: .defaultHigh, isActive: false),
                logoImageView.heightAnchor.constrain(to: 62, prioritizeAs: .defaultHigh, isActive: false)
            ]
        }
        constraints.append(logoImageView.getCentralizedXaxys(in: self))
        return constraints
    }
    
    fileprivate func activeLayoutConstraints(_ bool: Bool=true) {
        let searchBarConstraints = getConstraintsInSearchBar(for: isCompressed)
        let searchButtonConstraints = getConstraintsInSearchButton(for: isCompressed)
        let logoViewConstraints = getConstraintsInLogoView(for: isCompressed)
        
        searchBarConstraints.forEach({$0.isActive = bool })
        searchButtonConstraints.forEach({$0.isActive = bool})
        logoViewConstraints.forEach({$0.isActive = bool})
    }
    
}

