//
//  UIView+AutoLayout.swift
//  Test Mobile
//
//  Created by Renata Faria on 27/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import UIKit

//TODO: Add coments to this class
extension UIView {
    func setup(_ constraints: [NSLayoutConstraint]) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(constraints)
    }
    // XY axys
    /// Use this method to activate constraints to centralize in centerX and centerY of view in other. 
    /// You can use the default values or add your own.
    /// - Parameters:
    ///   - view: the view you are going to refer
    ///   - multiplierX: multiplier to X position
    ///   - constantX: constant to X position
    ///   - multiplierY: multiplier to Y position
    ///   - constantY: constant to Y position
    func centralizeXYaxys(in view: UIView, multiplierX: CGFloat=1, constantX: CGFloat=0, multiplierY: CGFloat=1, constantY: CGFloat=0) {
            centralizeXaxys(in: view, multiplier: multiplierX, constant: constantX)
            centralizeYaxys(in: view, multiplier: multiplierY, constant: constantY)
    }
    // x axys
    func centralizeXaxys(in view: UIView, multiplier: CGFloat=1, constant: CGFloat=0) {
        setup([getCentralizedXaxys(in: view, multiplier: multiplier, constant: constant) ])
    }
    func getCentralizedXaxys(in view: UIView, multiplier: CGFloat=1, constant: CGFloat=0) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: multiplier, constant: constant)
    }
    // y axys
    func centralizeYaxys(in view: UIView, multiplier: CGFloat=1, constant: CGFloat=0) {
        setup([getCentralizedYaxys(in: view, multiplier: multiplier, constant: constant) ])
    }
    func getCentralizedYaxys(in view: UIView, multiplier: CGFloat=1, constant: CGFloat=0) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: multiplier, constant: constant)
    }
}
