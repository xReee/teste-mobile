//
//  ViewCode.swift
//  Test Mobile
//
//  Created by Renata Faria on 27/05/20.
//  Copyright © 2020 Madeinweb. All rights reserved.
//

import Foundation

///Use this protocol to create view using code in an organized way
/**
**Methods:**
 - buildView: add views in super view
 - setupConstraints: setup views constraint here
 - setupAdditionalConfigs: use it to additional confuration like bg colors
 - setupView: this main method is used to call all view code methods
 */
protocol ViewCode {
    /// use this funtion to add views in super view
    func buildView()
    /// use this funtion to setup views constraint
    func setupConstraints()
    /// use this funtion to add additional confuration like bg colors
    func setupAdditionalConfigs()
    func setupView()
}

extension ViewCode {
    /// use this func to init view code in the corresponding view
    func setupView() {
        buildView()
        setupConstraints()
        setupAdditionalConfigs()
    }
}
